# Cumul.io filters

Allows filters to be applied to Cumul.io dashboards, by enabling applications to leverage Cumul.io's API.

Depends on https://www.drupal.org/project/cumulio.

Allows modules to alter the configuration properties of a dashboard before they are embedded.

See https://developer.cumul.io/#add-filters for more info.

## Usage
Refer to cumulio_filters.api.php