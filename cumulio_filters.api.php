<?php

/**
 * @file
 * API documentation for the cumulio_filters module.
 */

/**
 * Allows to override settings in order to provide securable id's for authorisation.
 *
 * Implements hook_cumulio_filters_settings_alter().
 *
 * @param array $settings
 *   An associative array of id's of securables to which you want to grant access to/
 *   @link https://developer.cumul.io/#add-filters for the full documentation.
 */
function hook_cumulio_filters_settings_alter(&$settings) {
}

/**
 * Allows to provide filter controls on dashboards.
 *
 * Implements hook_cumulio_filters_config_alter().
 *
 * @param $filters
 *   Alterable definition of filters, set criteria for filtering charts.
 *
 * @param $element
 *   Alterable field element which contains the dashboard. E.g. attach data attributes for JS processing
 *   or to control access.
 *
 * @param $dashboardId
 *   Dashboard ID set through configuration of cumulio module.
 *
 * @see https://developer.cumul.io/#add-filters for the full documentation.
 */
function hook_cumulio_filters_config_alter(&$filters, &$element, $dashboardId) {
}