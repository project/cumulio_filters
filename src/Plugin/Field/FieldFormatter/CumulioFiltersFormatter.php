<?php

namespace Drupal\cumulio_filters\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\cumulio\Plugin\Field\FieldFormatter\CumulioFormatter;
use Drupal\Core\Render\Markup;
use Drupal\Core\Template\Attribute;

/**
 * Plugin implementation of the 'cumulio_filters_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "cumulio_filters_formatter",
 *   label = @Translation("Cumulio filters formatter"),
 *   field_types = {
 *     "cumulio_field"
 *   }
 * )
 */
class CumulioFiltersFormatter extends CumulioFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = $element = [];
    // Config values.
    $config = \Drupal::config('cumulio.settings');
    $token = $config->get('api_token');
    $key = $config->get('api_key');

    foreach ($items as $delta => $item) {
      // Field value.
      $value = $item->getValue();
      $dashboardId = $value['value'];

      // Render container.
      $elements[$delta] = \Drupal::service('cumulio_filters.process_filters')->processSettings($dashboardId, $token, $key, $delta);
      // We don't want data-attributes to get overridden so we add them to raw output in viewValueFilters().
      $attributes = $elements[$delta]['#attributes'];
      unset($elements[$delta]['#attributes']);
      $elements[$delta]['#type'] = 'item';
      $elements[$delta]['#markup'] = $this->viewValueFilters($dashboardId, $attributes);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param string $dashboardId
   *   Cumul.io secure dashboard ID.

   * @param string $attributes
   *   Data attributes.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValueFilters($dashboardId, $attributes) {
    $attributes = new Attribute($attributes);
    $markup = "<div id=\"dashboard-$dashboardId\" $attributes></div>";
    return Markup::create($markup);
  }

}