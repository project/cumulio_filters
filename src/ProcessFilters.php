<?php

namespace Drupal\cumulio_filters;

/**
 * Class ProcessFilters
 *
 * @package Drupal\cumulio_filters
 *
 * @see \Drupal\cumulio_filters\Plugin\Field\FieldFormatter\CumulioFiltersFormatter
 *
 * @see Refer to Cumul.io documentation for more info: https://developer.cumul.io/#step-1-generate-an-authorization-token
 */
class ProcessFilters {

  /**
   * @var string
   */
  protected $dashboardId;

  /**
   * @var array
   */
  protected $element;

  /**
   * Process Cumul.io securables.
   *
   * @param string $dashboardId
   *   The ID of the dashboard to render.
   *
   * @param $token
   *   Cumul.io generated token
   *
   * @param $key
   *   Cumul.io generated key
   *
   * @param int $delta
   *   Field delta
   *
   * @return array
   *   Field item build for rendering
   *
   * @throws \exception
   */
  public function processSettings($dashboardId, $token, $key, $delta) {
    $this->dashboardId = $dashboardId;
    //@todo expose settings as system config manageable via the ui.
    $settings = ['type' => 'temporary', 'expiry' => (new \DateTime())->modify('+1 hours')->format('c'),
      'securables' => [$dashboardId]];
    // Allow settings array to be altered at module level.
    \Drupal::moduleHandler()->alter('cumulio_filters_settings', $settings);

    // Re-generate the token/key if filters are applied.
    if ($filters = $this->processFilters()) {
      // Additional settings to be considered during initialisation.
      if ($settings = array_merge($settings, $filters)) {
        // Generate a new token / key based on filters.
        $cumulio = \Drupal::service('cumulio_filters.cumulio')::initialize($key, $token);
        $authorization = $cumulio->create('authorization', $settings);
        $token = $authorization['token'];
        $key = $authorization['id'];
      }
    }

    // Add token/key dom objects for JavaScript processing.
    if (empty($token) || empty($key)) {
      throw new \exception('Unable to generate Cumul.io token or key!');
    }

    $build = [
      '#attached' => [
        // Attached the main JS file once.
        'library' => $delta === 0 ? 'cumulio_filters/embed-dashboards' : '',
      ],
      // Set token/key based on filters applied.
      '#attributes' => [
        'class' => 'cumulio-field-item',
        'data-cumulio-token' => $token,
        'data-cumulio-key' => $key,
        'data-cumulio-id' => $this->dashboardId
      ]
    ];

    $build = is_array($this->element) ? array_merge($build, $this->element) : $build;

    return $build;
  }

  /**
   * @inheritdoc
   */
  protected function processFilters() {
    //@todo expose filter definitions as system config manageable via the ui.
    $filters['filters'] = [];
    // Allow filters to be altered at module level.
    // Additionally you can alter the viewElement field properties by setting $element
    // based on Drupal render arrays documentation.
    \Drupal::moduleHandler()->alter('cumulio_filters_config', $filters['filters'], $element, $this->dashboardId);
    // Set additional properties for the field element.
    $this->element = $element;
    return !empty($filters['filters']) ? $filters : FALSE;
  }
}
