(function (d, a, s, h, b, oa, rd) {
    if (!d[b]) {
        oa = a.createElement(s), oa.async = 1;
        oa.src = h;
        rd = a.getElementsByTagName(s)[0];
        rd.parentNode.insertBefore(oa, rd);
    }
    d[b] = d[b] || {};
    d[b].addDashboard = d[b].addDashboard || function (v) {
        (d[b].list = d[b].list || []).push(v)
    };
})(window, document, 'script',
    'https://cdn-a.cumul.io/js/cumulio.min.js', 'Cumulio');

(function ($, Drupal) {
    /**
     * Implementation of Drupal behaviors.
     * @type {{attach: Drupal.behaviors.embedDashbaord.attach}}
     */
    Drupal.behaviors.embedDashbaord = {
        attach: function (context, settings) {
            $('.cumulio-field-item').each(function () {
                Cumulio.addDashboard({
                    language: settings.path.currentLanguage,
                    dashboardId: $(this).attr('data-cumulio-id'),
                    container: '#dashboard-' + $(this).attr('data-cumulio-id'),
                    token: $(this).attr('data-cumulio-token'),
                    key: $(this).attr('data-cumulio-key')
                });
            });
        }
    }
})(jQuery, Drupal);